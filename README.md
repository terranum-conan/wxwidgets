# wxwidgets

Change the version number in conanfile.py (line 85)

Build with :

    conan create . terranum-conan+wxwidgets/stable

For Linux, build with :

    conan create . terranum-conan+wxwidgets/stable

For Linux (before 3.2.0)

    conan create . terranum-conan+wxwidgets/stable -o wxwidgets:webview=False

The latest 3.2.0 uses GTK3.0 instead of GTK2. Thus we don't need to remove webview.

For Linux (v.3.2.2.1, 3.2.3 and 3.2.4)

    conan create . terranum-conan+wxwidgets/stable -o wxwidgets:png="system"

Using static libpng on linux leads to a crash when trying to display menu and icons.

For Linux (v.3.2.3)

    conan create . terranum-conan+wxwidgets/stable -o wxwidgets:png="system"

Append the following in your CMakelist.txt

        if(UNIX AND NOT APPLE)
	        find_package(PNG REQUIRED)
	        target_link_libraries(${PROJECT_NAME} ${PNG_LIBRARY})
        endif(UNIX AND NOT APPLE)

For Linux 3.2.3 append the `xkbcommon` library to avoid linking problems

Upload with :

    conan upload wxwidgets/3.2.1@terranum-conan+wxwidgets/stable --remote=gitlab -q 'build_type=Release'

## Build locally

    conan source . --source-folder=_bin
    conan install . --install-folder=_bin -o wxwidgets:webview=False
    conan build . --source-folder=_bin --build-folder=_bin
    conan export-pkg . terranum-conan+wxwidgets/stable --source-folder=_bin --build-folder=_bin
    conan test test_package wxwidgets/3.2.1@terranum-conan+wxwidgets/stable


## Windows

Using latest conan version (1.51.2) leads to problem (experimental feature Version is removed...) Running the following command :

        conan install libtiff/4.4.0@  FAILED

The same commands works with conan 1.51

For now, we have to force the conan version (`pipx install conan==1.51`)

## OSX

Very latest osx clang version (v.14) isn't supported by conan for now. Download and install previous "Command Line Tools for Xcode 13.4" from Apple developer website and run the following command to activate: `sudo xcode-select --switch /Library/Developer/CommandLineTools`

## Cross-building

        conan create . terranum-conan+wxwidgets/stable --profile:build=default --profile:host=intel_cross

This is the intel_cross profile:

        [settings]
        os=Macos
        arch=x86_64
        build_type=Release
        compiler=apple-clang
        compiler.libcxx=libc++
        compiler.version=13
        [options]
        [conf]
        [build_requires]
        [env]

An error appears when linking because "-Darwin" is added at the end of each library name. There is probably an error in the conanfile.py file used to build wxwidgets. For the moment the way around this problem is to remove "-Darwin" from the file names (in ".../package/lib") before uploading them to gitlab. 